# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.checkin.models import Checkin

class CheckinAdmin(admin.ModelAdmin):
    list_display = ('foto', 'nome')
    list_filter = ['nome']
    search_fields = ['titulo', 'hashtag', 'tags']

    def foto(self, obj):
        if obj.logo:
            return u'<img height="50px" src="/media/uploads/%s" />' % (obj.logo)
        else:
            return "Sem Imagem"
    foto.short_description = 'Logo'
    foto.allow_tags = True

admin.site.register(Checkin, CheckinAdmin)