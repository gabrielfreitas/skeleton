# -*- coding: utf-8 -*-
from django.db import models

class Checkin(models.Model):

    class Meta:
        verbose_name = 'Check-in de Vôo'
        verbose_name_plural = 'Check-in de Vôos'

    nome = models.CharField('Companhia Aerea', max_length=100)
    logo = models.ImageField(upload_to='uploads')
    link = models.TextField(help_text="Link da página de check-in da companhia aérea.")

    def __unicode__(self):
        return self.nome